/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sistema;


import java.text.*;
import java.util.*;

/**
 *
 * @author Julio
 */
public class Contato {
    private long idContato;
    private long idFkCaderno;
    private String nome;
    private String telCel;
    private String telFixo;
    private String end;
    private String Cidade;
    private String email;
    private Date dataNac = new Date();
    private String observacao;
    SimpleDateFormat sdf1= new SimpleDateFormat("dd/MM/yyyy");

    // ------------set ------
    public void setIdFkCaderno(long idFkCaderno) {
        this.idFkCaderno = idFkCaderno;
    }

    public void setCidade(String Cidade) {
        this.Cidade = Cidade;
    }
    public void setDataNac(Date dataNac) {
        this.dataNac = dataNac;
    }
    
    public void setDataNac(String date){
        System.out.println(date);
        try{
        sdf1.setLenient (false); 
        if(!date.equals(""))
            this.dataNac = sdf1.parse (date);
        else
            this.dataNac = null;
        }catch(Exception ex){
             ex.printStackTrace();            
        }
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
    public void setEnd(String end) {
        this.end = end;
    }
    public void setIdContato(long idContato) {
        this.idContato = idContato;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }
    public void setTelCel(String telCel) {
        this.telCel = telCel;
    }
    public void setTelFixo(String telFixo) {
        this.telFixo = telFixo;
    }
    
//   get---
    
    public long getIdFkCaderno() {
        return idFkCaderno;
    }

    public String getCidade() {
        return Cidade;
    }
    public String getStringDataNac() {
        if(dataNac != null)
            return sdf1.format(dataNac);
        return null;
    }
    
    public Date getDataNac(){
        return dataNac;
    }
    
    public String getEmail() {
        return email;
    }
    public String getEnd() {
        return end;
    }
    public long getIdContato() {
        return idContato;
    }
    public String getNome() {
        return nome;
    }
    public String getObservacao() {
        return observacao;
    }
    public String getTelCel() {
        return telCel;
    }
    public String getTelFixo() {
        return telFixo;
    }
}
