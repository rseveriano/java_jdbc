/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sistema;
/**
 *
 * @author Julio
 */
public class Caderno {
    private long id;
    private String nome;

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Caderno other = (Caderno) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

   
    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public void setId(long idCaderno) {
        this.id = idCaderno;
    }


    public String getNome() {
        return nome;
    }
    

    public long getId() {
        return id;
    }
    
}
