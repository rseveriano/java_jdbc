/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sistema;

import Banco.ContatoCRUD;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Julio
 */
public class ControleTabelaContato {
    private final DefaultTableModel TContato;
    private final javax.swing.JTable jTableResultadoPesquisa;
    private ArrayList<Contato> contatos;
    private Caderno caderno;
    
    public ControleTabelaContato(DefaultTableModel dtm,javax.swing.JTable jt){
        this.TContato = dtm;
        this.jTableResultadoPesquisa = jt;
    }   

    public ArrayList<Contato> getContatos() {
        return contatos;
    }
       
    public void iniTContato(){
        try{
        while(TContato.getRowCount()>0){
            TContato.removeRow(0);
        }
        jTableResultadoPesquisa.revalidate();
        contatos.forEach((c) -> {
            TContato.addRow(new Object[]{c.getNome(),
                c.getTelFixo(),
                c.getTelCel(),
                c.getEnd(),
                c.getDataNac(),
                c.getCidade(),
                c.getEmail()});
            });
        }catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Erro ao abrir os registros");    
        }
    }
    
    public void atualizarTcontato(){
        carregarTContato(caderno);
    }
    public void carregarTContato(Caderno c) {
        caderno = c;
        try{
        ContatoCRUD crud= new ContatoCRUD();
        contatos = crud.lerTudo(c); 
        iniTContato();
        }catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Erro ao tentar carregar os contatos");    
        }
    }
    
    public void carregarContatosFiltro(String campo, String valor){
        try{
        ContatoCRUD crud= new ContatoCRUD();
        contatos = crud.lerFiltro(caderno,campo,valor); 
        iniTContato();
        }catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Erro ao tentar carregar os contatos");    
        }       
    }
    
    public void inserirContatos(Contato c){
        try{
            new ContatoCRUD().inserir(c,caderno);
            carregarTContato(caderno);
            JOptionPane.showMessageDialog(null, "Contato criado na agenda:"+caderno.getNome());    
        }catch(Exception ex){
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Erro ao tentar criar contato");    
        }
    }
    
    
    public void atualizarContatos(Contato c){
        try{
            new ContatoCRUD().atualizar(c,caderno);
            carregarTContato(caderno);
            JOptionPane.showMessageDialog(null, "Contato salvado");    
        }catch(Exception ex){
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Erro ao salvar o contato");    
        }        
    }
    
    public void excluirContatos(int p){
         try{
            new ContatoCRUD().excluir(contatos.get(p),caderno);
            carregarTContato(caderno);
            JOptionPane.showMessageDialog(null, "Contato excluido na agenda:"+caderno.getNome());    
        }catch(Exception ex){
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Erro ao tentar excluir contato");    
        }       
    }
}
