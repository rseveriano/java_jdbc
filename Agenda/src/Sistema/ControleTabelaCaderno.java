/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sistema;

import Banco.CadernoCRUD;
import Interface.CriarAgendaView;
import Interface.EscolherAgendaView;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Julio
 */
public class ControleTabelaCaderno {
    private final DefaultTableModel TCaderno;
    private final javax.swing.JTable jTableAgendas;
    private final ControleAbasContato controlAbaCon;
    private ArrayList<Caderno> Agendas;
    
    public ControleTabelaCaderno(DefaultTableModel dtm,javax.swing.JTable jt,ControleAbasContato cac){
        this.TCaderno = dtm;
        this.jTableAgendas = jt;
        this.controlAbaCon = cac;
    }

    public ArrayList<Caderno> getAgendas() {
        return Agendas;
    }

    
    //-----------------atualiza Caderno-------------
    public void iniTCaderno(){
        try{
            while(TCaderno.getRowCount()>0){
                TCaderno.removeRow(0);
            }
            jTableAgendas.revalidate();
            CadernoCRUD crudCaderno = new CadernoCRUD();
            Agendas = crudCaderno.ler();
            for(Caderno c:Agendas)
                TCaderno.addRow(new Object[]{c.getNome()});
        
        }catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Erro ao tentar buscar as agendas");    
        }
        controlAbaCon.bloquearLiberarCampos(false);
    }
    public void inserirCadernos(){
        CriarAgendaView criarAgenda = new CriarAgendaView();
        criarAgenda.setVisible(true);
        String cad = criarAgenda.getNome();
        criarAgenda.dispose();
        if((cad != null) && (!cad.equals(""))){
            try{
                CadernoCRUD crudCaderno = new CadernoCRUD();
                crudCaderno.inserir(cad);
                iniTCaderno();
                JOptionPane.showMessageDialog(null, "Agenda criada");
            }catch (Exception ex) {
                ex.printStackTrace();
                JOptionPane.showMessageDialog(null, "Erro ao tentar criar a agenda");    
            }
        }else if((cad != null) && (cad.equals(""))){
            JOptionPane.showMessageDialog(null, "nome invalido");
        }
    }
    
    public void excluirCadernos(){
        EscolherAgendaView apagarAgenda = new EscolherAgendaView();
        apagarAgenda.setVisible(true);
        Caderno cad = apagarAgenda.getCaderno();
        apagarAgenda.dispose();
        if(cad != null){
            try{
                new CadernoCRUD().excluir(cad);
                iniTCaderno();
                JOptionPane.showMessageDialog(null, "Agenda excluida");
            }catch (Exception ex) {
                ex.printStackTrace();
                JOptionPane.showMessageDialog(null, "Erro ao tentar excluir a agenda");    
            }
        }       
    }
    public void atualizarCaderno(){
        EscolherAgendaView atualizarAgenda = new EscolherAgendaView();
        atualizarAgenda.setVisible(true);
        Caderno cadAtu = atualizarAgenda.getCaderno();
        atualizarAgenda.dispose();
        if(cadAtu !=null){
            CriarAgendaView nomeAgenda = new CriarAgendaView();
            nomeAgenda.setVisible(true);
            String cadNome = nomeAgenda.getNome();
            nomeAgenda.dispose();
            if(cadNome != null){
                try{
                    cadAtu.setNome(cadNome);
                    new CadernoCRUD().atualizar(cadAtu);
                    iniTCaderno();
                    JOptionPane.showMessageDialog(null, "Agenda alterada");
                }catch (Exception ex) {
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(null, "Erro ao tentar alterar a agenda");    
                }            
            }
    } 
    }
    
}
