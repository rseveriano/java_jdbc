package Banco;

import Sistema.Caderno;
import java.sql.SQLException;
import java.util.ArrayList;


public class CadernoCRUD extends MeuDAO{
	
    public void inserir(String nome) throws  Exception{
        if(nome != null){
            openDatabase();
            String sql = "INSERT INTO caderno(nome) VALUES ('"+nome+"')";
            stmt = com.createStatement();
            stmt.executeUpdate(sql); 
            clouseDatabase();
        }
    }

    public ArrayList<Caderno> ler()throws Exception{
        ArrayList<Caderno> c = new ArrayList();
        openDatabase();
        String sql = "SELECT * FROM caderno";
        stmt = com.createStatement();
        rs = stmt.executeQuery(sql);

        while (rs.next()){
        Caderno cad = new Caderno();
        cad.setId(rs.getLong("idCaderno"));
        cad.setNome(rs.getString("nome"));
        c.add(cad);
        
        }
        clouseDatabase();
        return c;
    }

    public void atualizar(Caderno c)throws Exception{
        if(c != null){
            openDatabase();
            String sql = "UPDATE caderno SET nome='"+c.getNome()+"' WHERE caderno.idCaderno = "+c.getId();
            stmt = com.createStatement();
            stmt.executeUpdate(sql);
            clouseDatabase();
        }
    }

    public void excluir(Caderno c) throws Exception{
        if(c != null){
            openDatabase();
            String sql = "DELETE FROM contato WHERE contato.fk_id_Caderno ="+c.getId();
            String sqlcaderno = "DELETE FROM caderno WHERE caderno.idCaderno ="+c.getId();
            stmt = com.createStatement();
            stmt.executeUpdate(sql);
            stmt.executeUpdate(sqlcaderno);
            clouseDatabase();
        }
    }
	
}